package com.bmb.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.bmb.util.R;

public class SliderPreference extends Preference implements OnSeekBarChangeListener
{
	private int max = 100;
	private int initialValue = 10;
	private int interval = 5;
	private int sliderValue;
	private Context context;
	
	public SliderPreference(Context context)
	{
		super(context);
		this.context = context;
	}
	
	public SliderPreference(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
	}

	public SliderPreference(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		this.context = context;
	}

	@Override
	protected View onCreateView(ViewGroup parent)
	{	
		LayoutInflater inflater = (LayoutInflater)context.getSystemService
	      (Context.LAYOUT_INFLATER_SERVICE);
		
		LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.slider_preference_layout, null);
		TextView title = (TextView) layout.findViewById(R.id.slider_preference_title);
		title.setText(getTitle());
		
		TextView summary = (TextView) layout.findViewById(R.id.slider_preference_summary);
		summary.setText(getSummary());
		
		SeekBar slider = (SeekBar) layout.findViewById(R.id.slider);
		slider.setMax(max);
		slider.setProgress(sliderValue);
		slider.setOnSeekBarChangeListener(this);		
		return layout; 
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser)
	{	
		progress = Math.round(((float)progress)/interval)*interval;
		
		if(!callChangeListener(progress))
		{
			seekBar.setProgress(sliderValue);
			return; 
		}

		seekBar.setProgress(progress);
		sliderValue = progress;
		updatePreference(progress);
		
		//notifyChanged();
	}
	
	public void setInterval(int interval)
	{
		this.interval = interval;
	}
	
	public void setMaxValue(int max)
	{
		this.max = max;
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar)
	{
	}
	
	@Override
	public void onStopTrackingTouch(SeekBar seekBar)
	{
	}


	@Override 
	protected Object onGetDefaultValue(TypedArray ta,int index)
	{
        return ta.getInt(index,initialValue);
	}


	@Override
	protected void onSetInitialValue(boolean restoreValue, Object defaultValue)
	{	
		int temp = restoreValue ? getPersistedInt(initialValue) : (Integer)defaultValue;
		
		if(!restoreValue)
		{
			persistInt(temp);
		}
		
		sliderValue = temp;
	}	

	private void updatePreference(int newValue)
	{	
		SharedPreferences.Editor editor =  getEditor();
		editor.putInt(getKey(), newValue);
		editor.commit();
	}

}