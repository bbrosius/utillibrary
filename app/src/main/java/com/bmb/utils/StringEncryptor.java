package com.bmb.utils;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 */
public class StringEncryptor
{
    // bytes used for the encryption key, MAKE SURE YOU CHANGE THESE VALUES
    private static byte[] keyBytes = new byte[] { 0x0f, 0x0a, 0x07, 0x03, 0x04, 0x15, 0x06, 0x17, 0x02, 0x03,
            0x01, 0x06, 0x12, 0x0a, 0x0e, 0x00, 0x10, 0x11, 0x03, 0x09, 0x14, 0x05, 0x01, 0x07 };


    // encrypt the clearText, base64 encode the cipher text and return it.
    public static String encrypt(String clearText) throws Exception {
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

        // init cipher
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        byte[] cipherText = cipher.doFinal(clearText.getBytes());
        return Base64.encodeToString(cipherText, Base64.DEFAULT);
    }

    public static String decrypt(String cipherText) throws Exception {
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

        // init cipher
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, keySpec);
        byte[] clearText = cipher.doFinal(Base64.decode(cipherText.getBytes(), Base64.DEFAULT));

        return new String(clearText);
    }
}
