package com.bmb.utils;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import com.bmb.util.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 */
public class ImageUtils
{
    public static int REQUEST_IMAGE_CAPTURE = 8050;

    /**
     * Sends off an image capture intent with intent code 8050. Returns a uri to the file where the image will be stored.
     * @param context The activity from which the function is being called, used to send the intent.
     * @param name The name for the image file, will be appended with _(TIMESTAMP)
     * @return The uri of the file where the image will be stored or null if the image file couldn't be created.
     */
    public static Uri dispatchTakePictureIntent(Activity context, String name) {
        Uri imageUri = null;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile(name);
            } catch (IOException ex) {
                Log.e(context.getString(R.string.log_tag), "Error creating file for picture: " + ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                // Save a file: path for use with ACTION_VIEW intents
                imageUri = Uri.fromFile(photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                context.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }

        return imageUri;
    }

    public static File createImageFile(String name) throws IOException
    {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = name + "_" + timeStamp;
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        return image;
    }

    /**
     * Adds the image at the provided uri to the gallery.
     * @param activity The calling activity used to send the broadcast
     * @param imageUri The uri of the image to add to the gallery.
     */
    public static void addPictureToGallery(Activity activity, Uri imageUri)
    {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(imageUri);
        activity.sendBroadcast(mediaScanIntent);
    }
}
