package com.bmb.utils;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditTextWithCursorDetection extends EditText
{
    public interface onCursorChangedListener {
        public void onCursorChanged(int selStart, int selEnd);
    }


    private onCursorChangedListener listener;

    public EditTextWithCursorDetection(Context context) {
        super(context);
    }

    public EditTextWithCursorDetection(Context context, AttributeSet attrs){
        super(context, attrs);
    }
    public EditTextWithCursorDetection(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
    }

    public void setOnCursorChangedListener(onCursorChangedListener o){
        listener = o;
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd){
        if( listener != null )
        {
            listener.onCursorChanged(selStart, selEnd);
        }
    }
}
